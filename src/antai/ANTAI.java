/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package antai;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author simon
 */
public class ANTAI extends Thread {

    /**
     * @param args the command line arguments
     */
    UI ui;
    ArrayList <Team> teams = new ArrayList();
    public ANTAI() {
        ui = new UI(this);
        for(int i = 0; i < 2; i++)
            teams.add(new Team(this, i, 200 * i + 100, 200));
        ui.start();
        this.start();
    }
    @Override
    public void run()
    {
        while(true)
        {
            try {
                this.sleep(10);
            } catch (InterruptedException ex) {
                Logger.getLogger(ANTAI.class.getName()).log(Level.SEVERE, null, ex);
            }
            for(Team t : teams)
                t.run();
        }
    }
    public static void main(String[] args) {
        ANTAI app = new ANTAI();
        
    }
    
}
