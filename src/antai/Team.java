/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package antai;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

/**
 *
 * @author simon
 */
public class Team {
    ANTAI app;
    ArrayList <Ant> ants = new ArrayList();
    double x, y;
    int color;
    public Team(ANTAI antai, int botType, double initx, double inity) {
        app = antai;
        x = initx;
        y = inity;
        color = (int)(Math.random() * 0xffffff);
        switch (botType) {
            case 0:
                ants.add(new YourAnt(app, this, x, y));
                break;
            case 1:
                ants.add(new EnemyAnt(app, this, x, y));
                break;
        }
    }
    public void draw(Graphics g) {
        g.setColor(new Color(color));
        for(Ant a : ants)
            a.draw(g);
    }

    void run() {
        for(Ant a : ants) {
            a.walk();
            a.run();
        }
    }
}
