/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package antai;

import java.awt.Graphics;
import java.util.ArrayList;

/**
 *
 * @author simon
 */
public abstract class Ant {
    ANTAI app;
    double x, y;
    double direction;
    byte speed;
    final double size = 10;
    Team team;
    public Ant(ANTAI antai, Team t, double initx, double inity) {
        app = antai;
        team = t;
        x = initx;
        y = inity;
    }
    public void draw(Graphics g) {
        g.fillOval((int)x, (int)y, (int)size, (int)size);
    }
    public void run()
    {
        x += 0.02 * speed * Math.sin(Math.toRadians(direction));
        y += 0.02 * speed * Math.cos(Math.toRadians(direction));
    }
    public abstract void walk();
    
}
