package antai;

import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author simon
 */
public class UI extends Thread {
    ANTAI app;
    Frame f;
    public UI(ANTAI antai) {
        app = antai;
        f = new Frame("ANT.A.I");
        f.setBounds(30, 30, 1000, 600);
        f.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent we)
            {
                System.exit(0);
             }
        });
        f.setVisible(true);
    }
    public void draw() 
    {    
        BufferedImage img = new BufferedImage(1000, 600, TYPE_INT_RGB);
        img.getGraphics().clearRect(0, 0, 1000, 600);
        for(Team t : app.teams)
            t.draw(img.getGraphics());
        f.getGraphics().drawImage(img, 0, 0, f);
    }      
    @Override
    public void run() {
        while(true)
            draw();
    }
}
