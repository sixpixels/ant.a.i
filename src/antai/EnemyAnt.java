/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package antai;

/**
 *
 * @author simon
 */
public class EnemyAnt extends Ant{

    public EnemyAnt(ANTAI antai, Team t, double initx, double inity) {
        super(antai, t, initx, inity);
    }

    @Override
    public void walk() {
        direction++;
        speed = 30;
    }
    
}
