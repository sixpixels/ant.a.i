# README #

# What is this repository for? #

This is a programming game which lets you control a colony of ants. These can search for food or battle other colonys.

Version: 0.0.1 - pre-alpha


# How do I get set up? #

Get the latest version of the Netbeans IDE (https://netbeans.org/) and the latest JDK.

### Developer ###

Clone the repository(Team -> Git -> Clone from Netbeans) and start coding :D.
If you are not part of the official developer team then please create a new Branch.

### Tester or Player ###

Go to Downloads and download "Latest-ANT.A.I.zip", extract it and open it as a Netbeans Project.
Look at the examples under "exampleAnts", and put your code into the "YourAnt.java". You don't have to look at the rest of the Project.


# Who do I talk to? #

If you've found some Bugs or have some proposals, please check if the issue already exists and comment it or create a new one, please add as much information as you know.
If you have any questions, check the wiki or contact the admin.